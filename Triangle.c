#include<stdio.h>
int n;//variable to store user input
int a=1;//start from first row
int b;
void row(int);//declaration of "row" function
int pattern(int);//declaration of "pattern" function

int pattern(int n)//function definition for number pattern
{
    if(n>=1)
    {
        row(a);
        printf("\n");
        a++;
        pattern(n-1);//recursive call of pattern function

    }
    else
    {
        return 1;
    }
}
void row(int b)//function definition for print lines
{
    if(b>=1)
    {
        printf("%d",b);
        row(b-1);//recursive call of row function
    }
}

int main()
{
    printf("Enter a number-:");//get the input of how many rows there should be
    scanf("%d",&n);
    pattern(n);
    return 0;
}
